package com.example.reactivepostgresdemo.repo;

import com.example.reactivepostgresdemo.model.Transaction;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface TransactionRepository extends ReactiveCrudRepository<Transaction, Integer> {
}



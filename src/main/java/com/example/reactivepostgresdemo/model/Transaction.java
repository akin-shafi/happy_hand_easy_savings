package com.example.reactivepostgresdemo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import java.util.Date;

@Table
@Data

public class Transaction {
    @Id
    private Integer id;
    @Column()
    private Integer customer_id;
    private String amount;
    private String trans_type;
    private String payment_method;
    private Date createdOn;

}

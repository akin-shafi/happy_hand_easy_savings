package com.example.reactivepostgresdemo.controller;

import com.example.reactivepostgresdemo.model.Transaction;
import com.example.reactivepostgresdemo.repo.TransactionRepository;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

    final
    TransactionRepository repository;

    public TransactionController(TransactionRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public Flux<Transaction> getTransaction(){
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<Transaction> getTransaction(@PathVariable Integer id){
        return repository.findById(id);
    }

    @PostMapping
    public Mono<Transaction> createTransaction(@RequestBody Transaction transaction){
        return repository.save(transaction);
    }

//    @PutMapping("/{id}")
//    public Mono<Transaction> updateTransaction(@RequestBody Transaction, @PathVariable Integer id){
//        return repository.findById(id)
//                .map((c) -> {
//                    c.setName(transaction.getName());
//                    return c;
//                }).flatMap( c -> repository.save(c));
//    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteTransaction(@PathVariable Integer id){
        return repository.deleteById(id);
    }
}
